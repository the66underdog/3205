import React from 'react';
import './style.css';

function ErrorNotif() {

    return (
        <>
            <div className='err-notif'>
                <h4>An error has been occured</h4>
                <span>Sent data is not defined in third-side API.<br />
                    <span className='text-imp'>Check the inputs</span></span>
            </div>
        </>
    );
}

export default ErrorNotif;