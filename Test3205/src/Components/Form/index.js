import React, { useEffect, useMemo, useState } from 'react';
import Quotation from '../Quotation';
import './style.css';

const MemoQuot = React.memo(Quotation);

function FormCur({ currencies, calc, children }) {

    const [currency, setCurrency] = useState();
    const [isDisabled, setIsDisabled] = useState(true);
    const [isErr, setIsErr] = useState(false);

    const value = useMemo(() => {
        return currency;
    }, [currency]);

    function parseStr(e) {
        if (!e.target.value.match(/\S/)) {
            return null;
        }
        const strArray = e.target.value.split(' ');
        const reqCond = strArray.length === 4 && strArray[0] !== '' && !isNaN(+strArray[0]) && strArray[1].match(/([a-z]|[A-Z]){3}/) && strArray[3].match(/([a-z]|[A-Z]){3}/);
        reqCond ? setIsDisabled(false) : setIsDisabled(true);
    }

    function checkCurs(e) {
        const formData = new FormData(e.target);
        const curStr = formData.get('input-currencies').split(' ');
        const curs = (curStr[1] + curStr[3]).toUpperCase();
        const cursReversed = (curStr[3] + curStr[1]).toUpperCase();
        currencies.includes(curs) || currencies.includes(cursReversed) ?
            calc(`https://currate.ru/api/?get=rates&pairs=${curs},${cursReversed}&key=e956de5272f4b44483280f25da16d0b5`, setCurrency, curStr[0]) : setIsErr(true);
    }

    useEffect(() => {
        isErr && setTimeout(() => setIsErr(false), 2000);
    }, [isErr]);

    return (
        <>
            <form className='form-currencies' onSubmit={(e) => { e.preventDefault(); checkCurs(e) }}>
                <fieldset>
                    <label htmlFor='input-currencies'>Request example: <span className='text-imp'>15 usd in rub</span></label>
                    <input type='text' name='input-currencies' placeholder='Enter the request' onChange={(e) => (parseStr(e))} />
                    <button type='submit' disabled={isDisabled}>Calculate</button>
                </fieldset>
                <div className='text-hint'>
                    <h3>Hint:</h3>
                    <span>To enable the submit button, input request-like text:</span>
                    <span>- first pattern must be an integer number;</span>
                    <span>- second and forth patterns must consist of 3 word characters;</span>
                    <span>- 'in' can be anything else.</span>
                </div>
            </form>
            {value && <MemoQuot curs={value} />}
            {isErr && children}
        </>
    );
}

export default FormCur;