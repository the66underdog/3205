import React from 'react';
import './style.css';

function Quotation({ curs }) {
    console.log('Quots');

    return (
        <div className='quotation-container'>
            <div className='quotation'>
                {curs && Object.keys(curs).map((key, index) => (<div key={index} className='block-quot'><h3>{key}</h3><span className='span-quot'>{curs[key]}</span></div>))}
            </div>
        </div>
    );
}

export default Quotation;