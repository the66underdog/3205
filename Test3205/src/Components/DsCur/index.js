import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import Quotation from '../Quotation';
import './style.css';

const MemQuotation = memo(Quotation);

function DsCur({ currencies, calc, children }) {
    console.log('Ds');

    const curs = useMemo(() => (
        ['RUB', 'USD', 'EUR']), []);
    const changeLang = useCallback(() => {
        if (navigator.language.match(/ru/i))
            return curs[0];
        if (navigator.language === 'en-US')
            return curs[1];
        if (navigator.language === 'en')
            return curs[2];
    }, [curs]);
    const [defaultCur, setDefaultCur] = useState(() => (changeLang()));
    const [quots, setQuots] = useState();
    const [isErr, setIsErr] = useState(false);

    const memQuots = useMemo(() => {
        return quots;
    }, [quots]);


    function checkQuot() {
        const regEx = new RegExp(defaultCur);
        const regExRev = new RegExp(`${defaultCur}([a-z]|[A-Z]){3}`)
        const arrBuf = [];
        for (let quot of currencies) {
            if (quot.match(regEx)) {
                if (quot.match(regExRev)) {
                    arrBuf.push(quot.substring(3) + defaultCur);
                }
                else
                    arrBuf.push(quot);
            }
        }
        calc(`https://currate.ru/api/?get=rates&pairs=${arrBuf.join()}&key=e956de5272f4b44483280f25da16d0b5`, setQuots);
    }

    useEffect(() => {
        curs.includes(defaultCur) ? checkQuot() : setIsErr(true);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [defaultCur]);

    useEffect(() => {
        isErr && setTimeout(() => setIsErr(false), 2000);
    }, [isErr]);

    return (
        <>
            <div className='quot-currencies'>
                <div className='quot-currencies-header'>
                    <h2>{defaultCur}</h2>
                    <input list='quot-datalist-options' name='quot-datalist' id='quot-datalist' onBlur={(e) => { setDefaultCur(e.target.value); }} />
                </div>
                {quots && <MemQuotation curs={memQuots} />}
                <datalist id='quot-datalist-options'>
                    {curs.map((cur, index) => (<option key={index} value={cur} />))}
                </datalist>
            </div>
            {isErr && children}
        </>
    );
}

export default DsCur;