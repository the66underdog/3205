import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import './App.css';
import FormCur from './Components/Form';
import DsCur from './Components/DsCur';
import ErrorNotif from './Components/ErrorNotif';

const MemFormCur = memo(FormCur);
const MemDsCur = memo(DsCur);

function App() {

  const [isFirst, setIsFirst] = useState(false);
  const [isSecond, setIsSecond] = useState(false);
  const [currencies, setCurrencies] = useState();

  const memCurrencies = useMemo(() => (currencies), [currencies]);

  function getCurrencies() {
    const req = new XMLHttpRequest();
    req.open('GET', '/currencies', true);
    req.onload = () => {
      if (req.status === 200) {
        const arrBuf = []
        for (let item of JSON.parse(req.response).data)
          arrBuf.push(item);
        setCurrencies(arrBuf);
      }
    }
    req.onerror = (err) => {
      throw err = new Error();
    }
    req.send();
  }

  const calculateCurrency = useCallback((url, cb, multiplier = 0) => {
    const req = new XMLHttpRequest();
    req.open('POST', '/currencies/calculate', true);
    req.onload = () => {
      if (req.status === 200) {
        const obj = JSON.parse(req.response).data;
        if (multiplier) {
          for (let key in obj)
            obj[key] = multiplier * obj[key];
        }
        cb(obj);
      }
    }
    req.onerror = (err) => {
      throw err = new Error();
    }
    req.send(url);
  }, []);

  useEffect(() => {
    getCurrencies();
  }, []);

  useEffect(() => {
    isFirst && setIsSecond(false);
  }, [isFirst]);

  useEffect(() => {
    isSecond && setIsFirst(false);
  }, [isSecond]);

  return (
    <div className="app">
      <div className='block-button-start'>
        <button className='button-start' onClick={() => setIsFirst(true)}>Enter the <span className='text-imp'>currency converter's</span> page</button>
        <button className='button-start' onClick={() => setIsSecond(true)}>Enter the <span className='text-imp'>currency quotations</span> data store</button>
      </div>
      {isFirst && <MemFormCur currencies={memCurrencies} calc={calculateCurrency}>
        <ErrorNotif />
      </MemFormCur>}
      {isSecond && <MemDsCur currencies={memCurrencies} calc={calculateCurrency}>
        <ErrorNotif />
      </MemDsCur>}
    </div>
  );
}

export default App;
