import exp from 'express';
import request from 'request';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const app = exp();

app.use(exp.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
    next();
});

app.get('/currencies', (req, res) => {
    request('https://currate.ru/api/?get=currency_list&key=e956de5272f4b44483280f25da16d0b5', (error, response, body) => {
        if (error || response.statusCode !== 200) {
            return res.status(500).json({ type: 'error', message: err.message });
        }

        res.send(body);
    });
});

app.post('/currencies/calculate', exp.text(), (req, res) => {
    request(req.body, (error, response, body) => {
        if (error || response.statusCode !== 200) {
            return res.status(500).json({ type: 'error', message: err.message });
        }
        res.send(body);
    });
});

app.listen(7000, (err) => {
    if (err)
        console.log('Error:' + err);
    else
        console.log(`It' fine`);
});

app.use((req, res) => {
    if (req.method === 'GET') {
        if (req.url === '/')
            loadPage(res, 'index.html', 'text/html');
        else {
            loadPage(res, req.url, getContentType(req.url));
        }
    }
});

function loadPage(res, name, conType) {
    const file = path.join(__dirname + '/../' + 'build/' + name);
    fs.readFile(file, (err, data) => {
        if (err) {
            res.writeHead(404);
            if (name.match('rows')) {
                res.write('No rows yet');
            }
        }
        else {
            res.writeHead(200, { 'Content-Type': conType });
            res.write(data);
        }
        res.end();
    })
}

function getContentType(url) {
    let ext = path.extname(url);
    switch (ext) {
        case '.html':
            return 'text/html';
        case '.css':
            return 'text/css';
        case '.js':
            return 'text/javascript';
        case '.json':
            return 'application/json';
        default:
            return 'multipart/form-data';
    }
}