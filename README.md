1) The main concept of this project is to connect to third-party API using an express node.js proxy server to pass CORS.
2) The primary purpose of this project is to get currencies' values and to show it to users.
3) Project consists of 2 SPAs:
    - a) Currency converter;
    - b) Currency's ratio to every other value available.

IMPORTANT: Keep in mind that third-party API has it's limits, so spamming request will lead to overflow the API's threshold.